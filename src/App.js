
import './App.css';
import TimeNow from './components/TimeNow';

function App() {
  return (
    <div className='main-container'>
      <TimeNow/>
    </div>
  );
}

export default App;

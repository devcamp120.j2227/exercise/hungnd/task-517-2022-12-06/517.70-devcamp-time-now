import { Component } from "react";

class TimeNow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            color: null,
            ouputMessage: []
        }
    }
    onBtnChangeColorClick = () => {
        const seconds = new Date().getSeconds();
        this.setState({
            ouputMessage: [...this.state.ouputMessage, this.getTime(new Date())]
        })
        if (seconds % 2 === 0)
            this.setState({
                color: "red"
            })
        else {
            this.setState({
                color: "blue"
            })
        }
    }
    getTime(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        seconds = seconds < 10 ? '0' + seconds : seconds;
        var strTime = hours + ':' + minutes + ':' + seconds + ' ' + ampm;
        return strTime;
    }
    render() {
        return (
            <div style={{ textAlign: "center"}}>
                <h1>Hello World!</h1>
                <p style={{color: this.state.color }}><b>It's {this.getTime(new Date())}</b></p>
                <button onClick={this.onBtnChangeColorClick}>Check Time</button>
                <div>
                    {this.state.ouputMessage.map((value, index) => {
                        index = index < 10 ? '0' + index : index;
                        return (
                            <p key={index}>{index}.{value}</p>
                        )
                    })}
                </div>
            </div>
        )
    }
}
export default TimeNow;